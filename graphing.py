# based on http://scipy-cookbook.readthedocs.io/items/Matplotlib_Gridding_irregularly_spaced_data.html

import numpy as np
from scipy.interpolate import griddata
import matplotlib
import matplotlib.pyplot as plt

import math
from random import random

debugMode = False

def isValidLine(line):
    if len(line) <= 1:
        return False
    info = line.split(",")
    if len(info) != 6 or info[2] == "nan" or info[3] == "nan" or info[2] == "0.0" or info[3] == "0.0":
        return False

    return True

def generateGraph(filename, networkName, exportName, showPoints=False):

    fig = plt.figure(dpi=250)

    points = []

    f = open(filename, "r")

    data = f.readlines()

    samples = {}

    for line in data:
        info = line.split(",")
        if not isValidLine(line):
            print("skipping invalid line: " + line)
            continue
    
        if not int(info[0]) in samples:
            samples[int(info[0])] = []

        samples[int(info[0])].append(info)

    '''
    samples is an dictionary of:
    {time: [samples], time2: [samples]}

    each [samples] list contains at least one sample per network, 
    and possibly more than one per network if more than one AP was detected
    '''

    # find the AP with the strongest signal from the network we are displaying, and add that to the list of points
    for sample in samples:
        bestSignal = 0
        strongestAP = []
        for network in samples[sample]:
            if network[4] != networkName:
                continue
            if int(network[5]) >= bestSignal:
                bestSignal = int(network[5])
                strongestAP = network

        # an AP with a signal was found
        if len(strongestAP) > 0:
            points.append([float(strongestAP[3]), float(strongestAP[2]), int(strongestAP[5])])
        else:
            # we recorded data at this point, but didn't see the network
            # record a zero value instead
            points.append([float(samples[sample][0][3]), float(samples[sample][0][2]), 0])

    x = []
    y = []
    z = []

    if debugMode:
        print points

    for point in points:
        x.append(point[0])
        y.append(point[1])
        z.append(point[2])

    # https://stackoverflow.com/a/2612815
    sortedX = list(x)
    sortedX.sort()
    sortedY = list(y)
    sortedY.sort()

    xMin = sortedX[0]
    xMax = sortedX[-1]
    yMin = sortedY[0]
    yMax = sortedY[-1]

    # define grid.
    xi = np.linspace(xMin, xMax, (xMax - xMin) / 0.00001)
    yi = np.linspace(yMin, yMax, (yMax - yMin) / 0.00001)

    # grid the data.
    zi = griddata((x, y), z, (xi[None, :], yi[:, None]), method='cubic')

    # if debugMode:
        # plot the contour lines
        # CS = plt.contour(xi, yi, zi, 15, linewidths=0.5, colors='k')

    CS = plt.contourf(xi, yi, zi, 15, cmap=plt.cm.jet, extent=(0, 1, 1, 0), alpha=1, vmin=0, vmax=60)

    if debugMode or showPoints:
        # plot data points
        plt.scatter(x, y, marker='o', c="white", linewidths=1, edgecolors="black", s=10)

    if debugMode:
        # cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ["blue", "green", "yellow", "red"], N=1025)
        # cmap, norm = matplotlib.colors.from_levels_and_colors([0, 10, 2, 30], ["orange", "green", "yellow"])

        plt.xlim(xMin, xMax)
        plt.ylim(yMin, yMax)
        plt.title('griddata test')

    if debugMode == False:
        # https://stackoverflow.com/a/24107230

        ax = fig.axes[0]
        ax.set_yticklabels([]); ax.set_xticklabels([]);ax.axis('off') 

    if debugMode:
        plt.show()
    else:
        fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
        fig.savefig(exportName, transparent=True, bbox_inches="tight", pad_inches=0)

if __name__ == "__main__":
    generateGraph("data.txt", "ISS_Students", "graph.png")

# if being run as a module, call generateGraph from the main script