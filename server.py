# http://www.tornadoweb.org/en/stable/

import tornado.web
import tornado.ioloop
import tornado.escape

import graphing
import time

import re

dataPath = ""
staticPath = ""
imagePath = ""

def isValidLine(line):
    if len(line) <= 1:
        return False
    info = line.split(",")
    if len(info) != 6 or info[2] == "nan" or info[3] == "nan" or info[2] == "0.0" or info[3] == "0.0":
        return False

    return True

def escapeNetworkName(network):
    return re.sub(r"[^a-zA-Z0-9-_]", "", network)

def getNetworkList(filename):
    networks = {}
    f = open(filename, "r")
    for line in f.readlines():
        if not isValidLine(line):
            continue
        networks[line.split(",")[4]] = True

    networksList = []
    for network in networks:
        networksList.append(network)

    f.close()

    return networksList

def getFormattedPoints(filename):
    f = open(filename, "r")

    validLines = []
    for line in f.readlines():
        if not isValidLine(line):
            continue
        if line.endswith("\n"):
            line = line[:-1] # remove newline character
        validLines.append(line)

    f.close()

    return validLines

def getDataBounds(filename):
    minLat = 9999
    maxLat = -9999
    minLon = 9999
    maxLon = -9999

    f = open(filename, "r")
    for line in f.readlines():
        if not isValidLine(line):
            continue
        data = line.split(",")
        lat = float(data[2])
        lon = float(data[3])

        if lat < minLat:
            minLat = lat
        if lat > maxLat:
            maxLat = lat
        if lon < minLon:
            minLon = lon
        if lon > maxLon:
            maxLon = lon

    f.close()

    return [[minLat, minLon], [maxLat, maxLon]]

class DataUpload (tornado.web.RequestHandler):
    def get(self):
        self.write("test")

    def post(self):
        print(self.get_body_argument("data"))
        f = open(dataPath, "a+")
        f.write(self.get_body_argument("data") + '\n')
        f.close()


class mapDisplay (tornado.web.RequestHandler):
    def get(self):
        resFile = open("static/index.html", "r")
        res = resFile.read()
        resFile.close()

        dataSource = "data/data.txt"

        try:
            networkName = self.get_query_argument("network")
        except Exception:
            networkName = "ISS_Students"

        if not networkName:
            networkName = "ISS_Students"
        fileName = "images/" + escapeNetworkName(networkName) + ".png"

        points = False
        try:
            showPointsQuery = self.get_query_argument("points")
            if showPointsQuery == "true":
                points = True
        except Exception:
            pass

        graphing.generateGraph(dataSource, networkName, fileName, showPoints=points)

        JSONData = {
            "networks": getNetworkList(dataSource),
            "selectedNetwork": networkName,
            "mapOverlay": fileName,
            "overlayBounds": getDataBounds(dataSource),
            "points": getFormattedPoints(dataSource)
        }

        res = res.replace("__serverData__", "window.serverData = " + tornado.escape.json_encode(JSONData))

        self.write(res)

# https://stackoverflow.com/a/18879658
class NoCachingFileHandler(tornado.web.StaticFileHandler):
    def set_extra_headers(self, path):
        self.set_header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')

def createServer():
    return tornado.web.Application([
        ("/upload", DataUpload),
        ("/", mapDisplay),
        (r'/static/(.*)', NoCachingFileHandler, {'path': staticPath}),
        (r'/images/(.*)', NoCachingFileHandler, {'path': imagePath}),
    ])


server = createServer()
server.listen(8080)
tornado.ioloop.IOLoop.current().start()
