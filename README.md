# WiFi Mapper

![](images/mapper.jpg)

The goal of this project was to collect data about WiFi networks using a Raspberry Pi. The device is connected to a battery, and after it is turned on, it records data to the attached SD card. After the data collection is complete, the data is transferred to a server, which analyzes the data and produces graphs of the relative strength of the detected WiFi signals.

## Assembling the device

### Components

|  |  |
| -- | -- |
|![](images/raspberrypi.jpg) | [Raspberry Pi 3B](https://www.amazon.com/Raspberry-Pi-RASPBERRYPI3-MODB-1GB-Model-Motherboard/dp/B01CD5VC92/) |
|![](images/sandisk.jpg) | [16GB micro SD card](https://www.amazon.com/SanDisk-Ultra-Micro-Adapter-SDSQUNC-016G-GN6MA/dp/B010Q57SEE) |
|![](images/heatsink.png) | [Heatsink](https://www.amazon.com/LoveRPi-Anodized-Performance-Heatsink-Raspberry/dp/B01J58A2DM/) |
|![](images/standoffs.png) | [Standoffs](https://www.amazon.com/Sutemribor-Female-Spacer-Standoff-Assortment/dp/B075K3QBMX/) |
|![](images/jumperwires.png) | [Jumper Wires](https://www.amazon.com/HiLetgo-Breadboard-Prototype-Assortment-Raspberry/dp/B077X7MKHN/) |
|![](images/breadboard.jpg) | [Miniature Breadboard](https://www.amazon.com/Phantom-YoYo-Points-Breadboard-Arduino/dp/B016Q6T7Q4/) |
|![](images/charger.png) | [Charging board with battery](https://www.amazon.com/Raspberry-Lithium-Battery-Expansion-Acrylic/dp/B06VT2Y59H/?th=1) |
|![](images/gps.jpg) | [Adafruit Ultimate GPS](https://www.adafruit.com/product/746) |
|![](images/ttl.jpg) | [USB to TTL Cable](https://www.adafruit.com/product/954) |
|![](images/display.jpg) | [128x32 Display](https://www.adafruit.com/product/661) |

### Assembly

* Attach the heatsinks to the Raspberry Pi.
* Solder the included header to the display and GPS.
* Solder a header to the Raspbery PI.
* Connect the GPS to the Pi by following [this diagram](https://learn.adafruit.com/assets/3713).
* Connect the display using [this diagram](https://learn.adafruit.com/ssd1306-oled-displays-with-raspberry-pi-and-beaglebone-black/wiring#spi-2-3).

----
* Print the parts included in the `case.f3d` file using a 3D printer.
* Use the standoffs to connect the charging board, Pi, and case together in the following order:
    * The flat portion of the case on the bottom.
    * The charging board, with the battery facing downwards.
    * The Pi, with the header facing up.
    * The top part of the case, with the screw posts facing down.
* Make sure the battery is charged, and connect it to the Raspberry Pi using the included MicroUSB cable.

### Software Installation

* Download a copy of [Raspbian](https://www.raspberrypi.org/downloads/raspbian/), and use [Etcher](https://etcher.io/) to copy it to a microSD card.
* Insert the microSD into the Raspberry Pi.
* Attach a monitor and keyboard to the Pi.
* Modify the keyboard layout, if necessary: type `sudo nano /etc/default/keyboard`, change the "XKBLAYOUT" property to "us", and reboot the Pi.
* Enable the Pi's SPI inferface using [this guide](https://www.raspberrypi-spy.co.uk/2014/08/enabling-the-spi-interface-on-the-raspberry-pi/).
* Install GPSD:
```
sudo apt-get install gpsd gpsd-clients python-gps
sudo systemctl stop gpsd.socket
sudo systemctl disable gpsd.socket
```
* Install the required dependencies for the display (see [this guide](https://learn.adafruit.com/ssd1306-oled-displays-with-raspberry-pi-and-beaglebone-black/usage) for more information):
```
sudo apt-get install build-essential python-dev python-pip
sudo pip install RPi.GPIO
sudo apt-get install python-imaging python-smbus
sudo apt-get install git

git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
sudo python setup.py install
```
* Create a new folder named `wifi_mapper`:
```
cd ~/
mkdir wifi_mapper
```

* Install the iwlist parser:
```
cd ~/wifi_mapper
git clone https://github.com/iancoleman/python-iwlist.git
mv ~/wifi_mapper/python-iwlist/iwlist.py ~/wifi_mapper/iwlist.py
```
* Copy `signalStrength.py` into this folder as well.
    * In this file, update the default `IPAddress` and `port` information to match the address of your server.

* To start collecting data:
```
gpsd /dev/ttyUSB0 -F /var/run/gpsd.sock
python ~/wifi_mapper/signalStrength.py
```


* Copy the repository onto the computer you wish to use as a server.
* Make sure Python 2.7.x is installed.
* Install tornado:
```
pip install tornado
```
* Install Scipy and and Matplotlib using [this guide](https://gist.github.com/klmr/2001569).
* Set the following variables inside `server.py`:
  * `dataPath` is the location of the text file where the uploaded data will be saved.
  * `staticPath` is the path to the `static` folder.
  * `imagePath` is the path to the `images` folder.
* Start the server:
```
python server.py
```
* By default, the server will use port 8080; this can be configured inside of server.py.

### Results

The server will generate a graph similar to the example shown below:

![](images/example_network_map.png)

Blue and green indicate a weaker network signal; yellow and red indicate a stronger signal.