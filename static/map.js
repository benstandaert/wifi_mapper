function updateQueryParam (name, value) {
  var old = new URLSearchParams(window.location.search)
  old.set(name, value)
  window.location.search = old.toString()
}

/* map */

L.mapbox.accessToken = ''

var imageUrl = serverData.mapOverlay
imageBounds = L.latLngBounds(serverData.overlayBounds)

var map = L.mapbox.map('map', 'mapbox.satellite')
  .fitBounds(imageBounds)

var overlay = L.imageOverlay(imageUrl, imageBounds)
  .addTo(map)

L.control.scale().addTo(map)

/* network selector */

var dropdown = document.getElementById('network-selector')

serverData.networks.sort(function (a, b) {
  return a.trim().toLowerCase() > b.trim().toLowerCase()
}).forEach(function (network) {
  var opt = document.createElement('option')
  opt.textContent = network
  if (network === serverData.selectedNetwork) {
    opt.selected = true
  }
  dropdown.appendChild(opt)
})

dropdown.addEventListener('change', function () {
  updateQueryParam('network', this.value)
})

var opacitySelector = document.getElementById('overlay-opacity-selector')

function setOpacity (value) {
  overlay.setOpacity(value)
  opacitySelector.value = value
}

setOpacity(0.45)

opacitySelector.addEventListener('input', function () {
  setOpacity(parseFloat(this.value))
})

/* point selector */

var points = serverData.points.map(function (line) {
  var data = line.split(',')

  return {
    lat: parseFloat(data[2]),
    lng: parseFloat(data[3]),
    network: data[4],
    signal: parseInt(data[5])
  }
})

function getNearestPoint (latlng) {
  var clone = []
  for (var i = 0; i < points.length; i++) {
    if (points[i].network !== serverData.selectedNetwork) {
      continue
    }
    var d = Math.sqrt(Math.pow(points[i].lat - latlng.lat, 2) + Math.pow(points[i].lng - latlng.lng, 2))
    if (d < 0.00033) { // limit the number of points that have to be sorted
      var copy = JSON.parse(JSON.stringify(points[i]))
      copy.distance = d
      clone.push(copy)
    }
  }
  clone.sort(function (a, b) {
    return a.distance - b.distance
  })
  return clone[0]
}

map.addEventListener('click', function (e) {
  console.log(e.latlng)
  var pt = getNearestPoint(e.latlng)

  if (pt) {
    var content = [pt.lat, pt.lng, pt.signal].join(', ')
    var location = {lat: pt.lat, lng: pt.lng}
  } else {
    var content = [e.latlng.lat, ', ', e.latlng.lng, '<br> <i>No Data</i>'].join('')
    var location = {lat: e.latlng.lat, lng: e.latlng.lng}
  }
  var popup = L.popup()
  popup.setLatLng(location)
  popup.setContent(content)
  popup.openOn(map)
})
