# sudo apt-get install git
# cd ~/wifi_mapper
# git clone https://github.com/iancoleman/python-iwlist.git
# mv ~/wifi_mapper/python-iwlist/iwlist.py ~/wifi_mapper/iwlist.py
# gpsd /dev/ttyUSB0 -F /var/run/gpsd.sock

import iwlist
import time
import operator

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306
import Image
import ImageDraw
import ImageFont

import subprocess
import gps
import threading
import sys

import urllib
import urllib2

import os

sampleRate = 5000

# open data log
logPath = "/home/pi/wifi_mapper/log.txt"
dataLog = open(logPath, "a+")

# where to upload data

IPAddress = ""
port = "8080"

# initialize display

display_RST = 24
display_DC = 23
display_SPI_PORT = 0
display_SPI_DEVICE = 0

disp = Adafruit_SSD1306.SSD1306_128_32(rst = display_RST, dc=display_DC, spi = SPI.SpiDev(display_SPI_PORT, display_SPI_DEVICE, max_speed_hz=8000000))
disp.begin()

font = ImageFont.load_default()

session = None

class GPSPollThread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		global session
		session = gps.gps("localhost", "2947")
		session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

	def run(self):
		global session
		global GPSThread
		while GPSThread.running:
			session.next()

def displayStatus(networks, GPSData):
	disp.clear()
	disp.display()

	image = Image.new("1", (disp.width, disp.height))
	drawing = ImageDraw.Draw(image)

	# https://stackoveflow.com/a/15188171
	sortedNetworks = networks
	sortedNetworks.sort(key=operator.itemgetter("signal_quality"), reverse=True)
	y = 0

	# display current position

	if GPSData == False:
		drawing.text((0, y), "unknown location", font=font, fill=255)
	else:
		drawing.text((0, y), str(GPSData["lat"]) + ", " + str(GPSData["lon"]), font=font, fill=255)
	y += 10

	# display top 2 strongest network signals

	for network in sortedNetworks[0:2]:
		drawing.text((0, y), network["essid"] + " - " + str(network["signal_quality"]), font=font, fill=255)
		y += 10

	disp.image(image)
	disp.display()

def getGPSData():
	return {"lat": session.fix.latitude, "lon": session.fix.longitude}

previousNetworks = []

def getNetworkData():
	data = iwlist.scan(inferface="wlan0")
	scanRes = iwlist.parse(data)

	networks = []
	for network in scanRes:
		networks.append({
			"essid": network["essid"],
			"signal_quality": network["signal_quality"]
		})
		if len(filter(lambda i: i["essid"] == network["essid"], previousNetworks)) == 0:
			previousNetworks.append({"essid": network["essid"]})
		
	for prev in previousNetworks:
		if len(filter(lambda i: i["essid"] == prev["essid"], networks)) == 0:
			networks.append({
				"essid": prev["essid"],
				"signal_quality": 0
			})

		print(previousNetworks)
		print("---")
		print(networks)
		print("\n\n")

		return networks

def takeSample():
	sampleTime = int(time.time() * 1000)

	data = iwlist.scan(interface="wlan0")
	print("got network data")
	GPSData = getGPSData()
	print("got GPS data")
	networks = getNetworkData()
	# print(networks)
	print(GPSData["lat"], GPSData["lon"])

	for network in networks:
		dataLog.write("%s,%s,%s,%s,%s,%s\n" % (sampleTime, sampleRate, GPSData["lat"], GPSData["lon"], network["essid"], network["signal_quality"]))

	displayStatus(networks, GPSData)

def uploadLog():
	print("uploading data")
	# https://stackoverflow.com/a/14639957
	dataLog.seek(0)
	data = dataLog.read()
	# print(data)
	print("http://" + IPAddress+ "/upload")
	requestData = urllib.urlencode([("data", data)])
	try:
		urllib2.urlopen("http://" + IPAddress + ":" + port + "/upload", requestData, timeout=15)
		# delete data if request succeeded
		dataLog.seek(0)
		dataLog.truncate()
		dataLog.write("")
	except:
		print("failed to upload data")
		pass

	timeSinceLastUpload = 0

timeSinceLastUpload = 0

# start polling GPS data

try:
	GPSThread = GPSPollThread()
	GPSThread.running = True
	GPSThread.start()

	while True:
		print("taking sample")
		takeSample()
		print("done taking sample")
		time.sleep(sampleRate / 1000)
		timeSinceLastUpload += sampleRate
		if timeSinceLastUpload > 5000:
		 	uploadLog()
except (KeyboardInterrupt, SystemExit, Exception):
	print("killing process")
	GPSThread.running = False
	GPSThread.join(2)
	print("exiting")
	sys.exit()